words => {
  let nice1 = 0;
  let nice2 = 0;

  for (const word of words.split('\n')) {
    if (!/ab|cd|pq|xy/g.test(word)) {
      if ((word.match(/[aeiou]/g) || []).length >= 3) {
        if (/(.)\1/.test(word)) {
          nice1++;
        }
      }
    }
    if (/(.).\1/.test(word)) {
      if (/(..).*\1/.test(word)) {
        nice2++;
      }
    }
  }

  return { A: nice1, B: nice2 };
};
