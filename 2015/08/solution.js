strings => {
  let codeTotal = 0;
  let stringTotal = 0;
  let encodedTotal = 0;

  for (const string of strings.split('\n')) {
    codeTotal += string.length;
    stringTotal += eval(string).length;
    encodedTotal += JSON.stringify(string).length;
  }
  return { A: codeTotal - stringTotal, B: encodedTotal - codeTotal };
};