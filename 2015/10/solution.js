initial => {
  const looksay = (said, rounds) => {
    for (let i = 0; i < rounds; i++) {
      said = said.match(/(\d)\1*/g).reduce((next, sequence) =>
        next + sequence.length + sequence[0]
      , '');
    }
    return said.length;
  }
  
  return { A: looksay(initial, 40), B: looksay(initial, 50) };
}