travel => {
  const costs = {};
  const locations = {};

  for (const trip of travel.split('\n')) {
    const [ stops, cost ] = trip.split (' = ');
    const [ a, b ] = stops.split(' to ');

    costs[`${a}->${b}`] = +cost;
    costs[`${b}->${a}`] = +cost;

    locations[a] = locations[a] || {};
    locations[a][b] = true;

    locations[b] = locations[b] || {};
    locations[b][a] = true;
  }

  const go = (origin, visited, cost, take) => {
    const destinations = locations[origin] || locations;
    let totals = [];

    for (const destination of Object.keys(destinations)) {
      if (visited[destination]) continue;
      totals.push(
        go(
          destination,
          Object.assign({ [destination]: true }, visited),
          cost + costs[`${origin}->${destination}`] || 0,
          take
        )
      );
    }
  
    return totals.length > 0 ?
      take(...totals) :
      cost;
  }

  return { A: go(null, {}, 0, Math.min), B: go(null, {}, 0, Math.max)};
}