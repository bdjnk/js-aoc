key => {
  let A = 1;
  while (!md5(key+A).startsWith('00000')) A++;

  let B = A;
  while (!md5(key+B).startsWith('000000')) B++;

  return { A, B };
}