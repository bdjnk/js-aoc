moves => {
  let x = 0;
  let y = 0;
  let visits = { '0,0': 1 };

  let x2a = 0;
  let y2a = 0;
  let x2b = 0;
  let y2b = 0;
  let visits2 = { '0,0': 2 };
  let turn = 'a';

  let xdelta = 0;
  let ydelta = 0;

  for (const move of moves) {
    switch (move) {
      case '^':
        ydelta = 1;
        break;
      case 'v':
        ydelta = -1;
        break;
      case '>':
        xdelta = 1;
        break;
      case '<':
        xdelta = -1;
        break;
    }

    x += xdelta;
    y += ydelta;
    visits[`${x},${y}`]++;

    switch (turn) {
      case 'a':
        x2a += xdelta;
        y2a += ydelta;
        visits2[`${x2a},${y2a}`]++;
        turn = 'b';
        break;
      case 'b':
        x2b += xdelta;
        y2b += ydelta;
        visits2[`${x2b},${y2b}`]++;
        turn = 'a';
        break;
    }
    xdelta = 0;
    ydelta = 0;
  }

  return { A: Object.keys(visits).length, B: Object.keys(visits2).length };
};
