directions => {
  let floor = 0;
  let basement = 0;
  let moves = 0;

  for (const char of directions) {
    moves++;
    switch (char) {
      case '(':
        floor++;
        break;
      case ')':
        floor--;
        if (basement === 0 && floor === -1) {
          basement = moves;
        }
        break;
    }
  }
  return { A: floor, B: basement };
};
