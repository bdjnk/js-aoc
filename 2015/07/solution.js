instructions => {
  const wires = {};

  const build = () => {
    for (const instruction of instructions.split('\n')) {
      const [input, id] = instruction.split(' -> ');
      wires[id] = input
    }
  }
  build();

  const get = item => /\d+/.test(item) ? item : track(item);

  const track = id => {
    const parts = wires[id].split(' ');
    let next;

    switch (parts[parts.length - 2]) {
      case 'AND':
        next = get(parts[0]) & get(parts[2]);
        break;
      case 'OR':
        next = get(parts[0]) | get(parts[2]);
        break;
      case 'LSHIFT':
        next = get(parts[0]) << parts[2];
        break;
      case 'RSHIFT':
        next = get(parts[0]) >> parts[2];
        break;
      case 'NOT':
        next = ~ get(parts[1]);
        break;
      default:
        next = get(parts[0]);
        break;
    }
    next = next & 0xFFFF;

    wires[id] = `${next}`;
    return next;
  }

  const A = track('a');

  build();
  wires.b = `${A}`;

  return { A, B: track('a') }
}