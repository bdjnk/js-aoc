boxes => {
  let wrapping = 0;
  let ribbon = 0;
  const numeric = (a, b) => a - b;

  for (const dimensions of boxes.split('\n')) {
    const [d1, d2, d3] = dimensions.split('x').sort(numeric);
    const [side1, side2, side3] = [d1 * d2, d1 * d3, d2 * d3];
    const surface = side1 * 2 + side2 * 2 + side3 * 2;
    wrapping += surface + side1;
    ribbon += d1 * 2 + d2 * 2 + d1 * d2 * d3;
  }
  return { A: wrapping, B: ribbon };
};