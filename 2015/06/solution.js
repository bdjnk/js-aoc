instructions => {
  const lights = [];
  let on = 0;
  let brightness = 0;

  for (let instruction of instructions.split('\n')) {
    const [ , command, colA, rowA, colB, rowB ]
      = instruction.match(/(^.+) (\d+),(\d+) through (\d+),(\d+)/);

    const fromCol = Math.min(colA, colB);
    const toCol = Math.max(colA, colB);
    const fromRow = Math.min(rowA, rowB);
    const toRow = Math.max(rowA, rowB);

    var dim = 0;

    for (let col = fromCol; col <= toCol; col++) {
      for (let row = fromRow; row <= toRow; row++) {
        if (lights[col] === undefined) lights[col] = [];
        if (lights[col][row] === undefined) lights[col][row] = { a: false, b: 0 };
        switch (command) {
          case 'turn on':
            on += lights[col][row].a ? 0 : 1;
            lights[col][row].a = true;
            dim = 1;
            break;
          case 'turn off':
            on += lights[col][row].a ? -1 : 0;
            lights[col][row].a = false;
            dim = lights[col][row].b > 0 ? -1 : 0;
            break;
          case 'toggle':
            on += lights[col][row].a ? -1 : 1;
            lights[col][row].a = !lights[col][row].a;
            dim = 2;
            break;
        }
        brightness += dim;
        lights[col][row].b += dim;
      }
    }
  }
  return { A: on, B: brightness };
};
