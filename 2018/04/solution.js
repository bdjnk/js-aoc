events => {
  const guards = {};
  let guard;
  let doze;

  for (const event of events.split('\n').sort()) {
    const [ , month, day, hour, minute, occurance ] =
      event.match(/\[1518-(\d+)-(\d+) (\d+):(\d+)\] (.*)/);

    switch (occurance.split(' ').pop()) {

      case 'shift':
        [ , guard ] = occurance.match(/#(\d+)/);
        if (guards[guard] === undefined)
          guards[guard] = { snoozing: 0, minutes: [], repeats: 0 };
        break;

      case 'asleep':
        doze = { month, day, hour, minute };
        break;

      case 'up':
        guards[guard].snoozing += (minute - doze.minute);

        for (let zzz = +doze.minute; zzz < +minute; zzz++) {

          if (guards[guard].minutes[zzz] === undefined) guards[guard].minutes[zzz] = 0;
          guards[guard].minutes[zzz]++;

          if (guards[guard].minutes[zzz] > guards[guard].repeats)
            guards[guard].repeats = guards[guard].minutes[zzz];
        }
        doze = undefined;
        break;
    }
  }

  const sleepiestGuard = Object.keys(guards).sort(
    (a, b) => guards[b].snoozing - guards[a].snoozing
  )[0];
  const snooziestMinuteA = guards[sleepiestGuard].minutes.reduce(
    (imax, minute, i, minutes) => minute > (minutes[imax] || -Infinity) ? i : imax, 0
  );

  const mostPredictableGuard = Object.keys(guards).sort(
    (a, b) => guards[b].repeats - guards[a].repeats
  )[0];
  const snooziestMinuteB = guards[mostPredictableGuard].minutes.reduce(
    (imax, minute, i, minutes) => minute > (minutes[imax] || -Infinity) ? i : imax, 0
  );

  return {
    A: sleepiestGuard * snooziestMinuteA,
    B: mostPredictableGuard * snooziestMinuteB
  };
};
