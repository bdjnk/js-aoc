coordinates => {
  let xmin = Infinity;
  let xmax = -Infinity;
  let ymin = Infinity;
  let ymax = -Infinity;

  const grid = [];
  const origins = [];

  const set = ({ x, y }, value) => {
    if (!grid[x]) grid[x] = [];
    grid[x][y] = value;
  };
  const get = ({ x, y }) => grid[x] ? grid[x][y] : undefined;

  for (const coordinate of coordinates.split('\n')) {
    const [ x, y ] = coordinate.split(', ').map(Number);
    if (x < xmin) xmin = x;
    if (x > xmax) xmax = x;
    if (y < ymin) ymin = y;
    if (y > ymax) ymax = y;

    const position = { x, y };
    const id = origins.length + 1;
    origins.push({ id, position, size: 1, beyond: 0 });
    set(position, { id, distance: 0, origin: true });
  }

  const gone = ({ x, y }) => y > ymax || y < ymin && x > xmax || x < xmin;

  const adjacent = (id, { x, y }) =>
    (get({ x: x+1, y }) || {}).id == id ||
    (get({ x: x-1, y }) || {}).id == id ||
    (get({ x, y: y+1 }) || {}).id == id ||
    (get({ x, y: y-1 }) || {}).id == id;

  let distance = 0;

  const claim = (id, position) => {
    if (!adjacent(id, position)) return;

    const value = get(position);
    if (value) {
      if (value.id && value.distance === distance) {
        const origin = origins[value.id-1];
        origin.size--;
        if (gone(position)) origin.beyond--;
        value.id = 0;
      }
    } else {
      set(position, { id, distance });
      const origin = origins[id-1];
      origin.size++;
      if (gone(position)) origin.beyond++;
    }
  };

  let remaining = origins.length;

  while (remaining > 0 && distance < 150) {
    distance++;
    for (const origin of origins) {
      if (origin.done) continue;

      let { id, position: { x, y }, size, beyond } = origin;

      y += distance;
      while (x < origin.position.x + distance) claim(id, { x: ++x, y: --y });
      while (y > origin.position.y - distance) claim(id, { x: --x, y: --y });
      while (x > origin.position.x - distance) claim(id, { x: --x, y: ++y });
      while (y < origin.position.y + distance) claim(id, { x: ++x, y: ++y });

      if (origin.size === size) {
        origin.done = true;
        remaining--;
      } else
      if (beyond > 0 && origin.beyond - beyond >= beyond && !origin.infinite) {
        origin.infinite = true;
        remaining--;
      }
    }
  }

  const largest_region = origins.reduce((size, origin) =>
    origin.done ? Math.max(size, origin.size) : size, 0
  );

  let cluster_area = 0;

  for (let y = ymin; y <= ymax; y++) {
    for (let x = xmin; x <= xmax; x++) {
      const distance = origins.reduce((distance, { position }) =>
        distance + Math.abs(x - position.x) + Math.abs(y - position.y), 0
      );
      if (distance < 10000) {
        cluster_area++;
      }
    }
  }

  return { A: largest_region, B: cluster_area};
};
