polymer => {

  const collapse = polymer => {
    const collapsed = [];
    for (let i = 0; i < polymer.length; i++) {
      const last = collapsed[collapsed.length - 1];
      if (last && Math.abs(last.charCodeAt(0) - polymer.charCodeAt(i)) === 32)
        collapsed.pop();
      else collapsed.push(polymer[i]);
    }
    return collapsed;
  };

  const collapsed = collapse(polymer);

  const units = new Set(polymer.toLowerCase());
  let shortest = Infinity;

  for (let unit of units) {
    const filtered = collapsed.filter(c => c.toLowerCase() !== unit);
    const len = collapse(filtered.join('')).length;
    if (shortest > len) shortest = len;
  }

  return { A: collapsed.length, B: shortest };
};