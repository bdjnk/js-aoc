tree => {
  const numbers = tree.split(' ').map(Number);

  const sum = (headerIndex = 0) => {
    let childrenCount = numbers[headerIndex];
    const valuesCount = numbers[headerIndex + 1];
    let childSumA;
    let childSumB;
    let childSums = [];
    let childrenSumA = 0;
    let nextIndex = headerIndex + 2;

    while (childrenCount > 0) {
      ({ nextIndex, A: childSumA, B: childSumB } = sum(nextIndex));
      childrenSumA += childSumA;
      childSums.push(childSumB);
      childrenCount--;
    }

    const nodeSumA = numbers
      .slice(nextIndex, nextIndex + valuesCount)
      .reduce((acc, val) => acc + val, 0);

    const nodeSumB = numbers
      .slice(nextIndex, nextIndex + valuesCount)
      .reduce((acc, i) => acc + (childSums[i - 1] || 0), 0);

    return {
      nextIndex: nextIndex + valuesCount,
      A: childrenSumA + nodeSumA,
      B: childSums.length ? nodeSumB : nodeSumA
    };
  };

  return { A: sum().A, B: sum().B };
};