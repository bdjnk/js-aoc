steps => {
  const getTasks = () => {
    const tasks = {};
    const todo = [];

    for (const step of steps.split('\n')) {
      const [ A, B ] = step.match(/\b\w\b/g);

      if (!tasks[A]) {
        tasks[A] = { id: A, needs: [], blocks: [], cost: A.charCodeAt(0) - 4 };
        todo.push(A);
      }
      tasks[A].blocks.push(B);

      if (!tasks[B]) {
        tasks[B] = { id: B, needs: [], blocks: [], cost: B.charCodeAt(0) - 4 };
        todo.push(B);
      }
      tasks[B].needs.push(A);
    }
    return { tasks, todo: todo.sort() };
  };

  let tasks;
  let todo;

  ({ tasks, todo } = getTasks());

  let order = '';
  while (todo.length > 0) {
    const i = todo.findIndex(id => tasks[id].needs.length === 0);
    order += todo[i];
    for (const block of tasks[todo[i]].blocks) {
      tasks[block].needs = tasks[block].needs.filter(need => need !== todo[i]);
    }
    todo.splice(i, 1);
  }

  ({ tasks, todo } = getTasks());

  let doing = [];
  let seconds = 0;
  const workers = 5;
  while (todo.length > 0 || doing.length > 0) {
    if (doing.length > 0) seconds++;
    doing = doing.reduce((still, id) => {
      tasks[id].cost--;
      if (tasks[id].cost > 0) still.push(id);
      else {
        for (const block of tasks[id].blocks) {
          tasks[block].needs = tasks[block].needs.filter(need => need !== id);
        }
      }
      return still;
    }, []);
    if (doing.length === workers) continue;
    todo = todo.reduce((still, id) => {
      if (tasks[id].needs.length === 0 && doing.length < workers) {
        doing.push(id);
      } else {
        still.push(id);
      }
      return still;
    }, []);
  }

  return { A: order, B: seconds };
};
