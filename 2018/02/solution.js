ids => {
  let twos = 0;
  let threes = 0;

  let base = '';
  let found = undefined;

  for (const id of ids.split('\n').sort()) {
    const counts = Object.values(
      id.split('').reduce((counts, letter) => {
        counts[letter] = counts[letter] ? counts[letter] + 1 : 1;
        return counts;
      }, {})
    );
    if (counts.includes(2)) twos++;
    if (counts.includes(3)) threes++;

    if (!found) {
      let divergance = 0;
      let j = undefined;
      for (let i = 0; i < Math.max(base.length, id.length); i++) {
        if (id[i] !== base[i]) {
          j = i;
          divergance++;
        }
        if (divergance > 1) {
          base = id;
          break;
        }
      }
      if (divergance === 1) found = id.slice(0, j) + id.slice(j+1);
    }
  }

  return { A: twos * threes, B: found };
};
