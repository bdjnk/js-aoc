claims => {
  const inches = [];
  let overlap = 0;

  for (const claim of claims.split('\n')) {
    const [ , xA, yA, w, h ] = claim.match(/@ (\d+),(\d+): (\d+)x(\d+)/);
    const xB = +xA + +w;
    const yB = +yA + +h;

    for (let x = +xA; x < xB; x++) {
      if (inches[x] === undefined) inches[x] = [];
      for (let y = +yA; y < yB; y++) {
        if (inches[x][y] === undefined) inches[x][y] = 1;
        else {
          if (inches[x][y] === 1) overlap++;
          inches[x][y]++;
        }
      }
    }
  }

  let uncontested;

  for (const claim of claims.split('\n')) {
    const [ , id, xA, yA, w, h ] = claim.match(/#(\d+) @ (\d+),(\d+): (\d+)x(\d+)/);
    const xB = +xA + +w;
    const yB = +yA + +h;

    uncontested = id;
    for (let x = +xA; x < xB; x++) {
      for (let y = +yA; y < yB; y++) {
        if (inches[x][y] > 1) uncontested = null;
      }
    }
    if (uncontested) break;
  }

  return { A: overlap, B: uncontested };
};
