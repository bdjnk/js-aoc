deltas => {
  const solution = { A: 0, B: null };

  deltas = deltas.split('\n').map(Number);

  solution.A = deltas.reduce((sum, n) => sum + n, 0);

  const seen = { 0: true };
  var frequency = 0;

  while (solution.B == null) {
    for (const delta of deltas) {
      frequency += delta;
      if (seen[frequency]) {
        solution.B = frequency;
        break;
      }
      else {
        seen[frequency] = true;
      }
    }
  }
  return solution;
};
