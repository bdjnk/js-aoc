programs => {
  const programsByName = {};
  for (const program of programs.split('\n')) {
    const [ you, childrenStr ] = program.split(' -> ');
    const [ , name, weight ] = you.match(/^(\w+) \((\d+)\)$/);
    const children = childrenStr ? childrenStr.split(', ') : [];
    if (!programsByName[name]) {
      programsByName[name] = { name };
    }
    programsByName[name].weight = Number(weight);

    const linkedChildren = children.map(childName => {
      const child = { name: childName };
      if (!programsByName[childName]) {
        programsByName[childName] = child;
      }
      programsByName[childName].parent = name;
      return programsByName[childName];
    });

    programsByName[name].children = linkedChildren;
  }
  const bottom = Object.values(programsByName).filter(program => !program.parent)[0];

  let correct = undefined;

  const recurse = node => {
    if (node.children.length === 0) {
      return Number(node.weight);
    }
    const weights = node.children.map(recurse);

    const counts = weights.reduce(
      (result, weight) => {
        result[weight] = (result[weight] || 0) + 1;
        return result;
      },
    {});
    if (Object.values(counts).length > 1 && !correct) {
      const wrong = Object.keys(counts).find(weight => counts[weight] === 1);
      const right = Object.keys(counts).find(weight => counts[weight] > 1);

      const i = weights.findIndex(weight => weight == wrong);
      correct = node.children[i].weight + (right - wrong);
    }
    return weights.reduce((total, weight) => total += weight, Number(node.weight));
  };
  recurse(bottom);

  return { A: bottom.name, B: correct };
};