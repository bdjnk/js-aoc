banklist => {
  const banks = banklist.split('\t').map(Number);
  const seen = {};
  const findimax = a => a.reduce((imax, v, i, a) => v > a[imax] ? i : imax, 0);
  let pattern = banks.join('');

  do {
    seen[pattern] = true;
    let index = findimax(banks);
    let hand = banks[index];
    banks[index] = 0;
    while (hand > 0) {
      index = (index + 1) % banks.length;
      banks[index]++;
      hand--;
    }
    pattern = banks.join('');
  } while (!seen[pattern]);

  let cycles = 0;

  do {
    let index = findimax(banks);
    let hand = banks[index];
    banks[index] = 0;
    while (hand > 0) {
      index = (index + 1) % banks.length;
      banks[index]++;
      hand--;
    }
    cycles++;
  } while (banks.join('') !== pattern);

  return { A: Object.keys(seen).length, B: cycles };
};
