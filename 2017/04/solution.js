passphrases => {
  const solution = {};
  solution.A = 0;
  solution.B = 0;

  for (const passphrase of passphrases.split('\n')) {
    solution.A++;
    solution.B++;
    const words = {};
    const anagrams = {};
    for (const word of passphrase.split(' ')) {
      const anagram = word.split('').sort().join('');
      if (words[word]) {
        solution.A--;
        solution.B--;
        break;
      }
      if (anagrams[anagram]) {
        solution.B--;
        break;
      }
      words[word] = true;
      anagrams[anagram] = true;
    }
  }

  return solution;
};
