spreadsheet => {
  const solution = {};
  solution.A = 0;
  solution.B = 0;

  for (const row of spreadsheet.split('\n')) {
    let rowMax = 0;
    let rowMin = Number.MAX_SAFE_INTEGER;
    for (const strValue of row.split('\t')) {
      const value = Number(strValue);
      if (value > rowMax) { rowMax = value; }
      if (value < rowMin) { rowMin = value; }

      for (const divisor of row.split('\t')) {
        if (divisor < value) {
          if (value % divisor === 0) {
            solution.B += value / divisor;
          }
        }
      }
    }
    solution.A += rowMax - rowMin;
  }
  return solution;
};
