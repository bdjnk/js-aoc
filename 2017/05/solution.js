jumplist => {
  const jumps = jumplist.split('\n').map(Number);
  let pos = 0;
  let steps = 0;

  while (jumps[pos] !== undefined) {
    const oldpos = pos;
    pos += jumps[pos];
    jumps[oldpos]++;
    steps++;
  }

  const jumps2 = jumplist.split('\n').map(Number);
  let pos2 = 0;
  let steps2 = 0;

  while (jumps2[pos2] !== undefined) {
    const oldpos = pos2;
    pos2 += jumps2[pos2];
    jumps2[oldpos] += jumps2[oldpos] < 3 ? 1 : -1;
    steps2++;
  }

  return { A: steps, B: steps2 };
};
