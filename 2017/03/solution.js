destination => {
  const solution = {};

  let right  = 1;
  let up     = 1;
  let left   = 1;
  let down   = 1;

  let outDistance = 0;
  let outValue = 0;

  for (;;) {
    right += 1+8*outDistance;
    up    += 3+8*outDistance;
    left  += 5+8*outDistance;
    down  += 7+8*outDistance;
    outDistance++;
    if (right >= destination) { outValue = right; break; }
    if (up >= destination) { outValue = up; break; }
    if (left >= destination) { outValue = left; break; }
    if (down >= destination) { outValue = down; break; }
  }

  solution.A = outDistance + (outValue - destination);

  const memory = {};
  let x = 0;
  let y = 0;
  memory[x] = {};
  memory[x][y] = 1;

  const nextDirection = {
    right: 'up',
    up: 'left',
    left: 'down',
    down: 'right'
  };

  let sideLength = 1;
  let sideDistance = 0;
  let sideDirection = 'right';

  for (;;) {
    switch (sideDirection) {
      case 'right':
        x++;
        break;
      case 'up':
        y++;
        break;
      case 'left':
        x--;
        break;
      case 'down':
        y--;
        break;
    }
    let nextValue = 0;

    for (let ox = x-1; ox <= x+1; ox++) {
      for (let oy = y-1; oy <= y+1; oy++) {
        if (memory[ox] && memory[ox][oy]) {
          nextValue += memory[ox][oy];
        }
      }
    }

    if (!memory[x]) {
      memory[x] = {};
    }
    memory[x][y] = nextValue;

    if (nextValue > destination) {
      break;
    }
    sideDistance++;
    if (sideDistance >= sideLength) {
      sideDirection = nextDirection[sideDirection];
      sideDistance = 0;
      if (sideDirection === 'right' || sideDirection === 'left') {
        sideLength++;
      }
    }
  }

  solution.B = memory[x][y];

  return solution;
};
