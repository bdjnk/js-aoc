input => {
  const solution = {};
  solution.A = 0;
  solution.B = 0;

  for (var i = 0, l = input.length; i < l; i++) {
    if (input[i] === input[( (i + 1) % l )]) {
      solution.A += +input[i];
    }
    if (input[i] === input[( ( i + (l / 2) ) % l )]) {
      solution.B += +input[i];
    }
  }
  return solution;
};
