# What?

`js-aoc`  is this repository containing my JavaScript solutions to [Advent of Code](https://adventofcode.com/) puzzles, but is also, and more importantly, the static [showcase website](http://bdjnk.gitlab.io/js-aoc/) for the aforementioned solutions.

## hyperHTML

The site is built using [hyperHTML](https://github.com/WebReflection/hyperhtml), but is likely very non-idiomatic and possibly subtly buggy, so read the hyperHTML code with skepticism. Any suggestions on improving it would be much appreciated.