const { bind, wire, Component } = hyperHTML;

function Code(code) {
  const highlight = event => {
    Prism.highlightElement(event.target);
  };
  return wire()`
    <pre><code data-call=code onconnected=${highlight} class=language-javascript>${code}</code></pre>
  `;
}

class AoC extends Component {

  constructor() {
    super();

    this.setState({
      all_years: ['2018', '2017', '2016', '2015'],
      all_days: [...Array(25).keys()].map(n => ('00'+(n+1)).slice(-2)),
      test: '',
    });

    this.considerURL();
    window.addEventListener('popstate', this.considerURL.bind(this));
  }

  considerURL() {
    const params = location.search.substr(1).split('&').reduce((p, q) => {
      const pair = q.split('=');
      p[pair[0]] = pair[1];
      return p;
    }, {});

    this.setState({
      selected_year: params.year,
      selected_day: params.day,
      code: undefined,
      input: undefined,
      result: undefined
    });

    this.load();
  }

  animate(key, animation, end = undefined, duration = 1) {
    this.setState({
      x: Object.assign({}, this.state.x, {
        [key]: `animate_${animation}`
      })
    });
    setTimeout(() => this.setState({
      x: Object.assign({}, this.state.x, { [key]: end })
    }), duration * 1000);
  }

  change_year(event) {
    const year = event.target.value.trim();
    
    this.setState({
      selected_year: year,
      selected_day: undefined,
      code: undefined,
      input: undefined,
      description: undefined,
      result: undefined
    });
    history.pushState({ year }, 'year', `?year=${year}`);
  }

  change_day(event) {
    const day = event.target.value.trim();
    const { selected_year: year } = this.state;

    this.setState({
      selected_day: day,
      code: undefined,
      input: undefined,
      description: undefined,
      result: undefined
    });
    history.pushState({ day }, 'day', `?year=${year}&day=${day}`);

    this.load();
  }

  load() {
    const {
      selected_year: year,
      selected_day: day
    } = this.state;

    if (!year || !day) {
      return;
    }
    const date = `${year}/${day}`;

    fetch(`${date}/solution.js`)
      .then(response => response.text())
      .then(code => {
        this.setState({ code: Code(code), solution: eval(code) });
      });

    const ext = all_solved[year][day] === '__' ? 'tst' : 'txt';

    fetch(`${date}/input.${ext}`)
      .then(response => response.text())
      .then(text => {
        this.setState({ input: text });
      });

    fetch(`${date}/description.txt`)
      .then(response => {
        if (!response.ok) throw new Error();
        return response.text();
      })
      .then(
        text => this.setState({ description: text }),
        () => this.setState({ description: undefined })
      );
  }

  solve() {
    const { test, input, solution } = this.state;
    this.setState({ result: solution(test || input) });
  }

  test(event) {
    this.setState({ test: event.target.value.trim() });
  }

  copy(event) {
    if (event.target.value !== '') {
      event.target.select();
      document.execCommand('copy');
      event.target.selectionStart = event.target.selectionEnd;
    }
    event.target.blur();
  }

  render() {
    const {
      all_years,
      all_days,
      selected_year,
      selected_day,
      code,
      input,
      description,
      result,
    } = this.state;

    const [explanation, question_one, question_two] = (description || "").split("\n");

    return this.html`
      <div id=main>
        <h1>JavaScript Advent of Code Solutions by Michael Plotke</h1>
        <p>
          <a
            href=${`http://adventofcode.com/${selected_year || ''}${selected_day ? `/day/${+selected_day}` : ''}`}
            target=_blank
          >
            Advent of Code Link ${selected_year ? `to ${selected_year}` : ''} ${selected_day || ''}
          </a>
        </p>
        ${wire()`
          Year
          <select data-call=change_year onchange=${this}>
            <option value=""></option>
            ${all_years.map(year => {
              const selected = selected_year !== year ? (selected_year ? 'gone' : '') : 'selected';
              const solved = all_solved[year] ? 'solved' : '';
              return wire()`
                <option value=${year} ?disabled=${!solved} ?selected=${selected_year == year}>${year}</option>
              `;
            })}
          </select>
        `}
        ${selected_year && wire()`
          Day
          <select data-call=change_day onchange=${this}>
            <option value=""></option>
            ${all_days.map(day => {
              const selected = selected_day !== day ? (selected_day ? 'gone' : '') : 'selected';
              const solved = all_solved[selected_year][day] ? 'solved' : '';
              return wire()`
                <option value=${day} ?disabled=${!solved} ?selected=${selected_day == day}>${day}</option>
              `;
            })}
        `}
        ${input && code && wire()`
          <button data-call=solve onclick=${this}>Run</button>
          ${description ? wire()`
            <p>${explanation}</p>
            <p>
              ${question_one}
              <br>
              <span class=result>${result ? result.A : wire()`<br>`}</span>
            </p>
            <p>
              ${question_two}
              <br>
              <span class=result>${result ? result.B : wire()`<br>`}</span>
            </p>
          ` : wire()`
            <p>
              Answer One: <span class=result>${result ? result.A : ''}</span><br>
              Answer Two: <span class=result>${result ? result.B : ''}</span>
            </p>
          `}
          <textarea id=test class=content placeholder="for test input" data-call=test onchange=${this}>
            ${this.state.test || input}
          </textarea>
          <div id=code class=content>
            ${code}
          </div>
        `}
      </div>
    `;
  }
}

bind(document.body)`${new AoC()}`;
